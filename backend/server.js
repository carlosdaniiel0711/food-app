const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults()
const jwt = require('jsonwebtoken')
const jwtSecret = 'food-node-api-secret'

// Set default middlewares (logger, static, cors and no-cache)
server.use(middlewares)

// Body parser
server.use(jsonServer.bodyParser)

// Middleware de autorização
server.use('/orders', (req, res, next) => {
    let requestToken = req.header('Authorization')

    console.log('Token: ' + requestToken);

    if(requestToken !== undefined && requestToken !== null){
        requestToken = requestToken.substring(7)

        jwt.verify(requestToken, jwtSecret, (error, decoded) => {
            if(decoded){
                next()
            } else {
                res.status(403).json({
                    erro: true,
                    message: 'Não autorizado. O token informado não é válido.'
                })
            }
        })
    } else {
        res.setHeader('WWW-Authenticate', 'Bearer token-type="JWT"')
        res.status(401).json({
            erro: true,
            message: 'Você precisa estar autenticado para acessar este recurso.'
        })
    }
})

// Login (autenticação)
server.post('/login', (req, res) => {
    let users = [
        {login: 'carlos.almeida', email: 'carlos.almeida@email.com', password: '123mudar'},
        {login: 'fulano.silva', email: 'fulano.silva@email.com', password: '123'}
    ]

    let isValid = false
    let body = req.body
    let user = null

    if(body !== undefined){
        user = users.find(x => x.login === body.login && x.password === body.password)
        isValid = (user !== undefined && user !== null) ? true : false
    }
    
    if(isValid){
        // Geração do token JWT
        let token = jwt.sign({
            sub: user.login,
            iss: 'food-node-api',
        }, jwtSecret)

        res.status(200).json({
            login: user.login,
            email: user.email,
            token: token
        })
    } else {
        res.status(403).json({
            erro: true,
            message: 'Dados inválidos.'
        })
    }
})

// Use default router
server.use(router)
server.listen(3000, () => {
  console.log('JSON Server is running!')
})
