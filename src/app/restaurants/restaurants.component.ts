import { Component, OnInit } from '@angular/core';

import {Restaurant} from './restaurant/restaurant.model'
import {RestaurantsService} from './restaurant/restaurants.service'

import { fadeInOnEnterAnimation } from 'angular-animations'

@Component({
  selector: 'food-restaurants',
  templateUrl: './restaurants.component.html',
  animations: [
    fadeInOnEnterAnimation()
  ]
})
export class RestaurantsComponent implements OnInit {	
  restaurants: Restaurant[]

  constructor(private restaurantsService: RestaurantsService) { 
  }

  ngOnInit() {
    this.restaurantsService.getRestaurants().subscribe((data: Restaurant[]) => this.restaurants = data)
  }
}
