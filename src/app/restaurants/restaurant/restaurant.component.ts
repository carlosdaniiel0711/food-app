import { Component, OnInit, Input } from '@angular/core';

import {Restaurant} from './restaurant.model'

import { pulseOnEnterAnimation } from 'angular-animations'

@Component({
  selector: 'food-restaurant',
  templateUrl: './restaurant.component.html',
  animations: [
    pulseOnEnterAnimation()
  ]
})
export class RestaurantComponent implements OnInit {
  @Input() restaurant: Restaurant

  constructor() { }

  ngOnInit() {
  }

}
