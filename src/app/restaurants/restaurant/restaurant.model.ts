export interface Restaurant {
	id: string
	name: string
	category: string
	deliveryTime: string
	rating: number
	logo: string
	about: string
	hours: string
}