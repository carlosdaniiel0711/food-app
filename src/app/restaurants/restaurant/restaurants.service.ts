import { Observable, throwError } from 'rxjs'
import { catchError } from 'rxjs/operators'

import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { Restaurant } from './restaurant.model'
import { Menu } from './../restaurant-details/restaurant-menu/menu.model'
import { Review, AddReviewDTO } from './../restaurant-details/restaurant-reviews/review.model'

import { API_ENDPOINT } from '../../app.api'
import { ErrorHandler } from '../../app.error-handler'

@Injectable()
export class RestaurantsService {

	constructor(private httpClient: HttpClient){
	}

	// obtem uma lista de restaurantes
	getRestaurants(): Observable<Restaurant[]> {
  		return this.httpClient.get<Restaurant[]>(`${API_ENDPOINT}/restaurants`)
  			.pipe(catchError(ErrorHandler.handleError))
	}

	// obtem um restaurante através do seu Id
	getRestaurantById(id: string): Observable<Restaurant> {
		return this.httpClient.get<Restaurant>(`${API_ENDPOINT}/restaurants/${id}`)
			.pipe(catchError(ErrorHandler.handleError))
	}

	// obtem o menu de um restaurante
	getRestaurantMenu(restaurantId: string): Observable<Menu[]> {
		return this.httpClient.get<Menu[]>(`${API_ENDPOINT}/restaurants/${restaurantId}/menu`)
			.pipe(catchError(ErrorHandler.handleError))
	}

	// obtem as avaliações de um restaurante
	getRestaurantReviews(restaurantId: string): Observable<Review[]> {
		return this.httpClient.get<Review[]>(`${API_ENDPOINT}/restaurants/${restaurantId}/reviews`)
			.pipe(catchError(ErrorHandler.handleError))
	}

	// adiciona uma review (comentário) para o restaurante
	addReview(restaurantId: string, review: AddReviewDTO): Observable<Review> {
		return this.httpClient.post<Review>(`${API_ENDPOINT}/restaurants/${restaurantId}/reviews`,
			JSON.stringify(review), { headers: new HttpHeaders().append('Content-Type', 'application/json') })
	}
}
