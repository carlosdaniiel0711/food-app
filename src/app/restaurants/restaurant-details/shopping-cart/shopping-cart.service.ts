import { Injectable } from '@angular/core'

import { Menu } from './../restaurant-menu/menu.model'
import { CartItem } from './cart-item.model'

@Injectable()
export class ShoppingCartService {
	cartItems: CartItem[] = [] // declarando um array vázio

	// adiciona um novo item no carrinho
	addItem(item: Menu): void {
		let foundItem = this.cartItems.find((x) => x.menuItem.id === item.id)

		if (foundItem){
			this.increaseQty(foundItem)
		} else{
			this.cartItems.push(new CartItem(item))
		}
	}

	// aumenta a quantidade de um item que está no carrinho
	increaseQty(item: CartItem): void {
		item.qty += 1
	}

	// diminui a quantidade de um item que está no carrinho
	decreaseQty(item: CartItem): void {
		item.qty -= 1

		if (item.qty == 0){
			this.removeItem(item)
		} 
	}

	// remove um item do carrinho
	removeItem(carItem: CartItem): void {
		this.cartItems.splice(this.cartItems.indexOf(carItem), 1)
	}	

	// limpa o carrinho (remove todos os itens)
	clearCart(): void {
		this.cartItems = []
	}

	// verifica se o item está no carrinho
	isOnCart(item: Menu): boolean {
		let foundItem = this.cartItems.find((x) => x.menuItem.id == item.id)

		if (foundItem){
			return true
		} else {
			return false
		}
	}

	// obtem o valor total do carrinho (soma o subtotal de todos os itens)
	getTotalCarrinho(): number {
		let total: number = 0
		
		for(let item of this.cartItems){
			total += item.getSubtotal()
		}
		
		return total
	}
}