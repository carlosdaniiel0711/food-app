import { Component, OnInit } from '@angular/core';

import { Menu } from './../restaurant-menu/menu.model'
import { CartItem } from './cart-item.model'
import { ShoppingCartService } from './shopping-cart.service'

import { swingOnEnterAnimation } from 'angular-animations'

@Component({
  selector: 'food-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  animations: [
    swingOnEnterAnimation()
  ]
})
export class ShoppingCartComponent implements OnInit {

  constructor(private service: ShoppingCartService) { }

  ngOnInit() {
  }

  // adiciona um item no carrinho
  addMenuItemOnCart(menuItem: Menu): void {
    this.service.addItem(menuItem)
  }

  // remove um item do carrinho
  removeFromCart(cartItem: CartItem): void {
    this.service.removeItem(cartItem)
  }

  // limpa o carrinho (remove todos os itens)
  clearCart(): void {
    this.service.clearCart()
  }

  // obtem os itens do carrinho de compras
  getCartItems(): CartItem[] {
  	return this.service.cartItems
  }

  // obtem o valor total do carrinho (soma o subtotal de todos os itens)
  getTotalCarrinho(): number {
  	return this.service.getTotalCarrinho()
  }

  // verifica se o carrinho está vázio
  isCarrinhoEmpty(): boolean {
    return this.getCartItems().length == 0
  }
}
