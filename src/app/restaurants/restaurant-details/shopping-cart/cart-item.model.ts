import { Menu } from './../restaurant-menu/menu.model'

export class CartItem {
	constructor(public menuItem: Menu, public qty: number = 1, public totalValue: number = 0) {
	}

	// obtem o subtotal do item (valor do produto x quantidade)
	getSubtotal(): number {
		return this.menuItem.price * this.qty
	}
}