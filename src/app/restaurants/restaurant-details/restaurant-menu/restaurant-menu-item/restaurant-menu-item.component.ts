import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Menu } from './../menu.model'
import { ShoppingCartService } from './../../shopping-cart/shopping-cart.service'

@Component({
  selector: 'food-restaurant-menu-item',
  templateUrl: './restaurant-menu-item.component.html'
})
export class RestaurantMenuItemComponent implements OnInit {
  @Input() menuItem: Menu
  @Output() addMenuItemEventEmitter = new EventEmitter()

  constructor(private shoppingCartService: ShoppingCartService) { }

  ngOnInit() {
  }

  addMenuItem(): void {
  	this.addMenuItemEventEmitter.emit(this.menuItem)
  }

  // verifica se o item está no carrinho
  isOnCart(): boolean {
    return this.shoppingCartService.isOnCart(this.menuItem)
  }
}
