export interface Menu {
	id: string
	logo: string
	name: string
	description: string
	price: number
	restaurantId: string
}