import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

import { Observable } from 'rxjs'

import { Menu } from './menu.model'
import { RestaurantsService } from './../../restaurant/restaurants.service'

import { fadeInOnEnterAnimation } from 'angular-animations'

@Component({
  selector: 'food-restaurant-menu',
  templateUrl: './restaurant-menu.component.html',
  animations: [
    fadeInOnEnterAnimation()
  ]
})
export class RestaurantMenuComponent implements OnInit {
  menus: Observable<Menu[]>

  constructor(private service: RestaurantsService, private route: ActivatedRoute) { 
  }

  ngOnInit() {
  	this.menus = this.service.getRestaurantMenu(this.route.parent.snapshot.params['id'])
  } 
}
