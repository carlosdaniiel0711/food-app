import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router'

import { Restaurant } from './../restaurant/restaurant.model'
import { RestaurantsService } from './../restaurant/restaurants.service'

import { fadeInOnEnterAnimation } from 'angular-animations'

@Component({
  selector: 'food-restaurant-details',
  templateUrl: './restaurant-details.component.html',
  animations: [
    fadeInOnEnterAnimation()
  ]
})
export class RestaurantDetailsComponent implements OnInit {
  restaurant: Restaurant

  constructor(private service: RestaurantsService, private route: ActivatedRoute) { 
  }

  ngOnInit() {
  	this.service.getRestaurantById(this.route.snapshot.params['id'])
  		.subscribe((data: Restaurant) => this.restaurant = data)
  }
}
