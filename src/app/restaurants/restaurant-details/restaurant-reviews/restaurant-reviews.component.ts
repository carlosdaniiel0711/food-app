import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

import { Observable } from 'rxjs'

import { Review } from './review.model'
import { RestaurantsService } from './../../restaurant/restaurants.service'

import { fadeInUpOnEnterAnimation, bounceOnEnterAnimation } from 'angular-animations'
import { LoginService } from 'src/app/security/login/login.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'food-restaurant-reviews',
  templateUrl: './restaurant-reviews.component.html',
  animations: [
    fadeInUpOnEnterAnimation(),
    bounceOnEnterAnimation()
  ]
})
export class RestaurantReviewsComponent implements OnInit {
  reviews: Review[]
  reviewForm: FormGroup

  constructor(private service: RestaurantsService, private route: ActivatedRoute, private loginService: LoginService, private fb: FormBuilder) {
    this.initForm()
  }

  ngOnInit() {
    this.service.getRestaurantReviews(this.route.parent.snapshot.params['id']).subscribe((dados: Review[]) => {
      this.reviews = dados
    })
  }

  initForm(): void {
    this.reviewForm = this.fb.group({
      rating: [5, [, Validators.min(0), Validators.max(5)]],
      comments: ['', Validators.required]
    })
  }

  isLogado(): boolean {
    return this.loginService.isLogado()
  }

  increaseRating(): void {
    const currentValue: number = this.rating.value
    let newValue: number = currentValue + 0.5

    if (newValue > 5) {
      newValue = 5
    }

    this.rating.setValue(newValue)
  }

  decreaseRating(): void {
    const currentValue: number = this.rating.value
    let newValue: number = currentValue - 0.5
    
    if (newValue < 0) {
      newValue = 0
    }

    this.rating.setValue(newValue)
  }

  getCurrentDate(): string {
    return new Date().toLocaleDateString()
  }

  enviarReview(): void {
    const restaurantId: string = this.route.parent.snapshot.params['id']

    this.service.addReview(restaurantId, this.reviewForm.value).subscribe((review: Review) => {
      this.reviewForm.reset({
        rating: 5,
        comments: ''
      })
      this.reviews.push(review)
    })
  }

  get comments() { return this.reviewForm.get('comments') }
  get rating() { return this.reviewForm.get('rating') }
}
