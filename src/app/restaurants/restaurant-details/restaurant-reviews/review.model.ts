export interface Review {
	name: string
	date: string
	rating: number
	comments: string
	restaurantId: string
}

export interface AddReviewDTO {
	rating: number,
	comments: string
}