import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http'

import swal from 'sweetalert2'

export class ErrorHandler {
	static handleError(error: HttpErrorResponse) {
		let message = error.error instanceof ErrorEvent ? 'Ocorreu um erro inesperado' 
			: error.error.message
		swal.fire('Erro', message, 'error')
		return throwError(message)
	}
}