import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { CartItem } from '../restaurants/restaurant-details/shopping-cart/cart-item.model'
import { Order } from './order.model'

import { Observable } from 'rxjs'

import { API_ENDPOINT } from './../app.api'

import { ShoppingCartService } from './../restaurants/restaurant-details/shopping-cart/shopping-cart.service'
import { LoginService } from '../security/login/login.service';

@Injectable()
export class OrderService {
	constructor(private shoppingCartService: ShoppingCartService, private http: HttpClient, private loginService: LoginService) {
	}

	getMyOrders(): Observable<Order[]> {
		return this.http.get<Order[]>(`${API_ENDPOINT}/orders`)
	}

	markAsDelivered(orderId: string): Observable<Order> {
		return this.http.put<Order>(`${API_ENDPOINT}/orders/finish/${orderId}`, null)
	}

	rateOrder(orderId: string, rating: number): Observable<Order> {
		return this.http.put<Order>(`${API_ENDPOINT}/orders/rate/${orderId}`, JSON.stringify({ rating }), 
			{ headers: new HttpHeaders().append('Content-Type', 'application/json') })
	}

	getCartItems(): CartItem[] {
		return this.shoppingCartService.cartItems
	}

	finalizaPedido(order: Order): Observable<any> {
		if(this.loginService.isLogado()){
			let data = JSON.stringify(order)
			
			return this.http.post<any>(`${API_ENDPOINT}/orders`, data, { headers: new HttpHeaders().append('Content-Type', 'application/json') })
		}
	}

	increaseQty(item: CartItem): void {
		this.shoppingCartService.increaseQty(item)
	}

	decreaseQty(item: CartItem): void {
		this.shoppingCartService.decreaseQty(item)
	}

	remove(item: CartItem): void {
		this.shoppingCartService.removeItem(item)
	}

	getTotalValue(): number {
		return this.shoppingCartService.getTotalCarrinho()
	}

	getEnderecoByCep(cep: string): Observable<Cep> {
		let apiEndpoint: string = `https://viacep.com.br/ws/${cep}/json`
		return this.http.get<Cep>(apiEndpoint)
	}

	clearCart(): void {
		this.shoppingCartService.clearCart()
	}
}

export interface Cep {
	logradouro: string
}