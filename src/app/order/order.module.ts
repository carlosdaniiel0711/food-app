import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderComponent } from './order.component';
import { OrderItemsComponent } from './order-items/order-items.component';
import { OrderSummaryComponent } from './order-summary/order-summary.component';
import { OrderService } from './order.service';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxMaskModule } from 'ngx-mask';
import { SharedModule } from '../shared/shared.module';
import { OrderDeactivateGuard } from './orderdeactivate.guard';
import { HttpRequestInterceptor } from '../http-request.interceptor';

export const ROUTES: Routes = [
  {path: '', component: OrderComponent, canDeactivate: [OrderDeactivateGuard]}
]

@NgModule({
  declarations: [
    OrderComponent,
    OrderItemsComponent,
    OrderSummaryComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    ReactiveFormsModule,
    HttpClientModule,
    NgxMaskModule.forRoot()
  ],
  providers: [
    OrderService,
    {provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true}
  ]
})
export class OrderModule { }
