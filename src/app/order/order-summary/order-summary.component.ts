import { Component, OnInit } from '@angular/core';

import { OrderService } from '../order.service'

@Component({
  selector: 'food-order-summary',
  templateUrl: './order-summary.component.html'
})
export class OrderSummaryComponent implements OnInit {

  constructor(private orderService: OrderService) { }

  ngOnInit() {
  }

  getTotalCarrinho(): number {
  	return this.orderService.getTotalValue()
  }

  getFrete(): number {
    // return this.getTotalCarrinho() * 0.03
    return 0
  }

  getTotalPedido(): number {
  	return this.getTotalCarrinho() + this.getFrete()
  }
}
