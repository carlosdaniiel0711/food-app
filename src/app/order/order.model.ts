import { CartItem } from './../restaurants/restaurant-details/shopping-cart/cart-item.model'

export interface Order {
	id: string,
	customer: string,
	address: string, 
	complement: string,
	paymentOption: string,
	totalValue: number,
	createdAt: string,
	completedAt: string,
	rating: number,
	status: string,
	items: CartItem[]
}