import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { CartItem } from './../../restaurants/restaurant-details/shopping-cart/cart-item.model'

@Component({
  selector: 'food-order-items',
  templateUrl: './order-items.component.html'
})
export class OrderItemsComponent implements OnInit {
  @Input() items: CartItem[]

  @Output() increaseQtyEventEmitter = new EventEmitter()
  @Output() decreaseQtyEventEmitter = new EventEmitter()
  @Output() removeEventEmitter = new EventEmitter()

  constructor() { }

  ngOnInit() {
  }

  isCartEmpty(): boolean {
  	return this.items.length == 0
  }

  increaseQty(item: CartItem): void {
  	this.increaseQtyEventEmitter.emit(item)
  }

  decreaseQty(item: CartItem): void {
  	this.decreaseQtyEventEmitter.emit(item)
  }

  remove(item: CartItem): void {
  	this.removeEventEmitter.emit(item)
  }
}
