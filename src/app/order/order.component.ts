import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, Form } from '@angular/forms'

import { RadioOption } from './../shared/radio-input/radio-option.model'
import { CartItem } from './../restaurants/restaurant-details/shopping-cart/cart-item.model'
import { Order } from './order.model'

import { OrderService, Cep } from './order.service'

import { fadeInOnEnterAnimation } from 'angular-animations'

import swall from 'sweetalert2'
import { Router } from '@angular/router';

@Component({
  selector: 'food-order',
  templateUrl: './order.component.html',
  animations: [
    fadeInOnEnterAnimation()
  ]
})
export class OrderComponent implements OnInit {
  paymentMethods: RadioOption[] = [
    {label: 'Dinheiro', value: 'DINHEIRO'},
    {label: 'Cartão de débito', value: 'DEBITO'},
    {label: 'Cartão de crédito', value: 'CREDITO'},
    {label: 'Cartão refeição', value: 'REFEICAO'}
  ]

  deliveryForm: FormGroup

  constructor(private orderService: OrderService, private fb: FormBuilder, private router: Router) {
    this.initForm()
  }

  ngOnInit() {
    
  }

  buscaEndereco(): void {
    let cep: string = this.cep.value
    
    if(cep !== null && cep !== ''){
      this.orderService.getEnderecoByCep(cep).subscribe((dados: Cep) => {
        this.endereco.setValue(dados.logradouro)
      }, err => {
        swall.fire('Erro', 'Esse CEP não foi encontrado. Verifique se o mesmo está no formato correto', 'error')
      })
    }
  }

  getCartItems(): CartItem[] {
    return this.orderService.getCartItems()
  }

  hasItems(): boolean {
    return this.orderService.getCartItems().length !== 0
  }

  increaseQty(item: CartItem): void {
    this.orderService.increaseQty(item)
  }

  decreaseQty(item: CartItem): void {
    this.orderService.decreaseQty(item)
  }

  remove(item: CartItem): void {
    this.orderService.remove(item)
  }

  finalizaPedido(): void {
    const order: Order = {
      id: null,
      customer: this.cliente.value,
      address: this.endereco.value,
      complement: this.complemento.value,
      paymentOption: this.paymentOption.value,
      items: this.getCartItems(),
      totalValue: 0,
      status: null,
      createdAt: null,
      completedAt: null,
      rating: 0
    }
    
    this.orderService.finalizaPedido(order).subscribe((dados: any) => {
      this.orderService.clearCart()
      swall.fire('Sucesso', `Pedido enviado com sucesso, agora é só aguardar! Número do pedido: ${dados['id']}`, 'success')
      this.router.navigate(['/restaurants'])
    })
  }

  initForm(): void {
    this.deliveryForm = this.fb.group({
      cliente: ['', Validators.required],
      cep: [''],
      endereco: ['', Validators.required],
      complemento: [''],
      paymentOption: ['', Validators.required]
    })
  }
  
  limpaForm(): void {
    this.deliveryForm.reset()
  }

  isFormValid(): boolean {
    return this.deliveryForm.valid && this.getCartItems().length != 0 
  }

  selectPaymentMethod(selectedOption: RadioOption): void {
    this.paymentOption.setValue(selectedOption.value)
  }

  get cliente() { return this.deliveryForm.get('cliente') }
  get cep() { return this.deliveryForm.get('cep') }
  get endereco() { return this.deliveryForm.get('endereco') }
  get complemento() { return this.deliveryForm.get('complemento') }
  get paymentOption() { return this.deliveryForm.get('paymentOption') }
}
