import { OrderComponent } from './order.component';
import { CanDeactivate, ActivatedRouteSnapshot } from '@angular/router';

import swal from 'sweetalert2'
import { Injectable } from '@angular/core';

@Injectable()
export class OrderDeactivateGuard implements CanDeactivate<OrderComponent> {
    
    canDeactivate(component: OrderComponent, currentRoute: ActivatedRouteSnapshot): Promise<boolean> | boolean {
        if(component.hasItems()){
			return new Promise<boolean>((returned, failure) => {
				swal.fire({
					title: 'Aviso',
					text: 'Deseja realmente sair desta tela? Os dados inseridos serão perdidos.',
					type: 'warning',
					confirmButtonText: 'OK',
					cancelButtonText: 'Cancelar',
					showCancelButton: true,
					reverseButtons: true
				}).then((result) => {
					returned((result.value) ? true : false)
				})
			})
		} else {
			return true
		}
    }
}