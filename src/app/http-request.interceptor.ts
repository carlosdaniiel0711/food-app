import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginService } from './security/login/login.service';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
    
    constructor(private loginService: LoginService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if(this.loginService.isLogado() && request.url.includes("localhost"))
        {
            const newRequest = request.clone({
                headers: request.headers.set('Authorization', `Bearer ${this.loginService.getUsuarioLogado().token}`)
            })
            
            return next.handle(newRequest)
        } else {
            return next.handle(request)
        }
    }
}