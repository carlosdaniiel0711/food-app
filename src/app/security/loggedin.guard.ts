import { CanLoad, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { LoginService } from './login/login.service';

@Injectable()
export class LoggedinGuard implements CanLoad, CanActivate {

    constructor(private loginService: LoginService){
    }

    canLoad(route: Route): boolean {
        return this.checkAuthentication()
    }

    canActivate(activatedRoute: ActivatedRouteSnapshot, routeState: RouterStateSnapshot): boolean {
        return this.checkAuthentication()
    }

    private checkAuthentication(): boolean {
        let isLogado: boolean = this.loginService.isLogado()

        if(!isLogado){
            this.loginService.handleLogin()
        }

        return isLogado
    }
}