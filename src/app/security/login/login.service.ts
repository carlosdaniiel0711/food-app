import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators'

import { Autenticacao } from './usuario.model';

import { API_ENDPOINT } from 'src/app/app.api';
import { Router, NavigationEnd } from '@angular/router';

@Injectable()
export class LoginService {
    private usuarioLogado: Autenticacao
    private returnUrl: string = '/home'

    constructor(private http: HttpClient, private router: Router) {
        this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((e: NavigationEnd) => {
                if(e.url !== '/login')
                    this.returnUrl = e.url
            })
    }

    public efetuaLogin(login: string, password: string): Observable<Autenticacao> {
        return this.http.post<Autenticacao>(`${API_ENDPOINT}/auth`, JSON.stringify({ loginOrEmail: login, password: password }), 
            { headers: new HttpHeaders().append('Content-Type', 'application/json') })
    }

    public setUsuarioLogado(usuario: Autenticacao): void {
        this.usuarioLogado = usuario
    }

    public isLogado(): boolean {
        return this.usuarioLogado !== undefined
    }

    public getUsuarioLogado(): Autenticacao {
        return this.usuarioLogado
    }

    public handleLogin(returnUrl: string = this.returnUrl): void {
        this.router.navigate(['/login'])
    }

    public handleLogout(): void {
        this.usuarioLogado = undefined
    }

    public getReturnUrl(): string {
        return this.returnUrl
    }
}