import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { Autenticacao } from './usuario.model';
import Swal from 'sweetalert2';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'food-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup

  constructor(private fb: FormBuilder, private loginService: LoginService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.initForm()
  }

  ngOnInit() {
  }

  initForm(): void {
    this.loginForm = this.fb.group({
      login: ['', Validators.required],
      senha: ['', Validators.required]
    })
  }

  efetuaLogin(): void {
    this.loginService.efetuaLogin(this.login.value, this.senha.value).subscribe((data: any) => {
      const auth: Autenticacao = {
        login: data.userData.login,
        email: data.userData.email,
        token: data.accessToken
      }

      Swal.fire('Sucesso', 'Usuário logado com sucesso!', 'success')
      this.loginService.setUsuarioLogado(auth)

      this.router.navigate([this.loginService.getReturnUrl()])
    }, (response: HttpErrorResponse) => {
      Swal.fire('Erro', response.error.message, 'error')
    })
  }

  get login() { return this.loginForm.get('login') }
  get senha() { return this.loginForm.get('senha') }
}
