export interface Autenticacao {
    login: string,
    email: string,
    token: string
}