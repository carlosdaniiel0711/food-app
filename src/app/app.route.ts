import {Routes} from '@angular/router'

import {HomeComponent} from './home/home.component'
import {RestaurantsComponent} from './restaurants/restaurants.component'
import {RestaurantDetailsComponent} from './restaurants/restaurant-details/restaurant-details.component'
import {RestaurantMenuComponent} from './restaurants/restaurant-details/restaurant-menu/restaurant-menu.component'
import {RestaurantReviewsComponent} from './restaurants/restaurant-details/restaurant-reviews/restaurant-reviews.component'
import { LoginComponent } from './security/login/login.component';
import { LoggedinGuard } from './security/loggedin.guard';
import { NotFoundComponent } from './not-found/not-found.component';
import { MyOrdersComponent } from './my-orders/my-orders.component'

export const ROUTES: Routes = [
	{path: '', component: LoginComponent},
	{path: 'login', component: LoginComponent},
	{path: 'home', component: HomeComponent},
	{path: 'restaurants/:id', component: RestaurantDetailsComponent,
		children: [
			{path: '', redirectTo: 'menu', pathMatch: 'full'},
			{path: 'menu', component: RestaurantMenuComponent},
			{path: 'reviews', component: RestaurantReviewsComponent}
		]
	},
	{path: 'restaurants', component: RestaurantsComponent},
	{path: 'order', loadChildren: './order/order.module#OrderModule', canLoad: [ LoggedinGuard ], canActivate: [ LoggedinGuard ] },
	{path: 'my-orders', component: MyOrdersComponent, canActivate: [ LoggedinGuard ] },
	{path: '**', component: NotFoundComponent}
]