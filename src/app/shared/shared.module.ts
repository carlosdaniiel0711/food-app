import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericInputComponent } from './generic-input/generic-input.component';
import { RadioInputComponent } from './radio-input/radio-input.component';

@NgModule({
  declarations: [
    GenericInputComponent,
    RadioInputComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    GenericInputComponent,
    RadioInputComponent
  ]
})
export class SharedModule { }
