import { Component, OnInit, ContentChild, AfterContentInit, Input } from '@angular/core';
import { FormControlName } from '@angular/forms';

@Component({
  selector: 'food-generic-input',
  templateUrl: './generic-input.component.html'
})
export class GenericInputComponent implements OnInit, AfterContentInit {
  @Input() label: string
  @Input() errorMessage: string
  @Input() showErrorMessage: boolean = true

  input: FormControlName

  @ContentChild(FormControlName) formControlName: FormControlName 

  constructor() { }

  ngOnInit() {
  }

  ngAfterContentInit() {
    this.input = this.formControlName
  }
}
