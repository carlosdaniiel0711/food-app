import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { RadioOption } from './radio-option.model'

@Component({
  selector: 'food-radio-input',
  templateUrl: './radio-input.component.html'
})
export class RadioInputComponent implements OnInit {
  @Input() options: RadioOption[]
  @Output() selectOptionEventEmitter = new EventEmitter()

  selectedOption: RadioOption

  constructor() { }

  ngOnInit() {
  	this.selectOption(this.options[0])
  }

  selectOption(option: RadioOption): void {
  	this.selectedOption = option
    this.selectOptionEventEmitter.emit(this.selectedOption)
  }
}
