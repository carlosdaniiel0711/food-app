import { Component, OnInit } from '@angular/core';
import { OrderService } from '../order/order.service';
import { Order } from '../order/order.model';

import swall from 'sweetalert2'

import { fadeInOnEnterAnimation, swingOnEnterAnimation } from 'angular-animations'

@Component({
  selector: 'food-my-orders',
  templateUrl: './my-orders.component.html',
  animations: [
    fadeInOnEnterAnimation(),
    swingOnEnterAnimation()
  ]
})
export class MyOrdersComponent implements OnInit {

  orders: Order[] = []

  constructor(
    private orderService: OrderService
  ) { }

  ngOnInit() {
    this.orderService.getMyOrders().subscribe((dados: Order[]) => this.orders = dados)
  }

  isEntregue(order: Order): boolean {
    return order.completedAt !== null
  }

  isRated(order: Order): boolean {
    return order.rating !== null
  }

  confirmarEntrega(order: Order): void {
    if (!this.isEntregue(order)) {
      this.orderService.markAsDelivered(order.id).subscribe((dados: Order) => {
        swall.fire('Pedido finalizado', 'Entrega confirmado com sucesso!', 'success')
        this.orders[this.orders.indexOf(order)] = dados
      })
    } else {
      swall.fire('Ação inválida', 'Este pedido já está entregue!', 'warning')
    }
  }

  async showAvaliacaoModal(order: Order) {
    const { value: rating } = await swall.fire({
      title: 'Avaliar pedido',
      type: 'info',
      input: 'radio',
      inputOptions: {
        '0': '0',
        '1': '1',
        '2': '2',
        '3': '3',
        '4': '4',
        '5': '5',
      },
      inputValidator: (value: string) => {
        if (!value) {
          return 'Escolha uma nota para o pedido'
        }
        
        const valueAsNumber: number = Number.parseInt(value)

        if (valueAsNumber < 0 || valueAsNumber > 5) {
          return 'Valor inválido. A nota varia de 0 (zero) à 5 (cinco)'
        }
      },
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: '<i class="fa fa-check" aria-hidden="true"></i> Enviar'
    })

    if (rating) {
      this.enviarAvaliacao(order, Number.parseInt(rating))
    }
  }

  enviarAvaliacao(order: Order, rating: number): void {
    if (!this.isRated(order)) {
      if (this.isEntregue(order)) {
        this.orderService.rateOrder(order.id, rating).subscribe((dados: Order) => {
          swall.fire('Obrigado', 'Pedido avaliado com sucesso! :)', 'success')
          this.orders[this.orders.indexOf(order)] = dados
        })
      } else {
        swall.fire('Ação inválida', 'Não é possível avaliar um pedido que ainda não foi entregue!', 'warning')
      }
    } else {
      swall.fire('Ação inválida', 'Este pedido já foi avaliado anteriormente!', 'warning')
    }
  }
}
