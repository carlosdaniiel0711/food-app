import { Component, OnInit } from '@angular/core';
import { LoginService } from '../security/login/login.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'food-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private loginService: LoginService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  login(): void {
    this.loginService.handleLogin()
  }

  getNomeUsuarioLogado(): string {
    return this.loginService.getUsuarioLogado().login
  }

  isLoggedIn(): boolean {
    return this.loginService.isLogado()
  }

  logout(): void {
    this.loginService.handleLogout()
  }
}
