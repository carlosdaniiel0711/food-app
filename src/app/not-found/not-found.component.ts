import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'food-not-found',
  templateUrl: './not-found.component.html'
})
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
