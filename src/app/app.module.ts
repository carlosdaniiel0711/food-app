import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule  } from '@angular/platform-browser/animations'
import { NgxMaskModule } from 'ngx-mask'

import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';

import {ROUTES} from './app.route'

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { RestaurantsComponent } from './restaurants/restaurants.component';
import { RestaurantComponent } from './restaurants/restaurant/restaurant.component';
import { RestaurantsService } from './restaurants/restaurant/restaurants.service';
import { RestaurantDetailsComponent } from './restaurants/restaurant-details/restaurant-details.component';
import { RestaurantMenuComponent } from './restaurants/restaurant-details/restaurant-menu/restaurant-menu.component';
import { RestaurantReviewsComponent } from './restaurants/restaurant-details/restaurant-reviews/restaurant-reviews.component';
import { RestaurantMenuItemComponent } from './restaurants/restaurant-details/restaurant-menu/restaurant-menu-item/restaurant-menu-item.component';
import { ShoppingCartComponent } from './restaurants/restaurant-details/shopping-cart/shopping-cart.component';
import { ShoppingCartService } from './restaurants/restaurant-details/shopping-cart/shopping-cart.service';
import { LoginComponent } from './security/login/login.component'
import { LoginService } from './security/login/login.service';
import { LoggedinGuard } from './security/loggedin.guard';
import { SharedModule } from './shared/shared.module';
import { OrderDeactivateGuard } from './order/orderdeactivate.guard';
import { NotFoundComponent } from './not-found/not-found.component';
import { HttpRequestInterceptor } from './http-request.interceptor';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { OrderService } from './order/order.service';

registerLocaleData(ptBr)

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    RestaurantsComponent,
    RestaurantComponent,
    RestaurantDetailsComponent,
    RestaurantMenuComponent,
    RestaurantReviewsComponent,
    RestaurantMenuItemComponent,
    ShoppingCartComponent,
    LoginComponent,
    NotFoundComponent,
    MyOrdersComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES, {preloadingStrategy: PreloadAllModules}),
    SharedModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgxMaskModule.forRoot()
  ],
  providers: [
    RestaurantsService, 
    ShoppingCartService,
    LoginService,
    LoggedinGuard,
    OrderDeactivateGuard,
    OrderService,
    {provide: LOCALE_ID, useValue: 'pt-BR'},
    {provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
